
setwd("D:/R/Lab2")
source("regression.R")

# data = read.table("../30-banknote.txt", TRUE)
# regression(data$skewness, data$curtosis)

n = 10000
a = -10
b = -10
s2 = 1
x = seq(0.0, 1.0, length = n)
y = a * x * b + rnorm(n, 0, sqrt(s2))
regression(x, y)