# skewness + curtosis
require(MASS)
source('analyseCor.R')

setwd("D:/R/Lab1")
data <- read.table('30-banknote.txt', TRUE)

# 30
n = 10000
a = c(1, -1)
R = cbind(c(16, -15), c(-15, 16))
data_test = mvrnorm(n, a, R)

# Корреляционный анализ на живых
# analyseCor(data$skewness, data$curtosis)

# Корреляционный анализ на тестовых
# analyseCor(data_test[,1], data_test[,2])
